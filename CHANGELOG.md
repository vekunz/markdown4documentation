# Changelog

## ##Next Release##

* \<Bug|Feature|Improvement>: \<Title of the issue or feature> \<link to issue> (\<link to merge request>)

## v0.5.0

* Bug: Empty heading numbering template displayed as undefined [#13](https://gitlab.com/vekunz/markdown4documentation/issues/13)
* Feature: Add standalone headings in configuration [#14](https://gitlab.com/vekunz/markdown4documentation/issues/14) ([!6](https://gitlab.com/vekunz/markdown4documentation/merge_requests/6))
* Improvement: Set the 'Creator' tag of generated PDFs [#16](https://gitlab.com/vekunz/markdown4documentation/issues/16)
* Feature: Support fenced code blocks [#3](https://gitlab.com/vekunz/markdown4documentation/issues/3) ([!7](https://gitlab.com/vekunz/markdown4documentation/merge_requests/7))

## v0.4.0

* Improvement: \\\< should be escaped [#8](https://gitlab.com/vekunz/markdown4documentation/issues/8) ([!4](https://gitlab.com/vekunz/markdown4documentation/merge_requests/4))
* Bug: Default templates not found [#11](https://gitlab.com/vekunz/markdown4documentation/issues/11)
* Feature: Automatically add numbers to headings [#12](https://gitlab.com/vekunz/markdown4documentation/issues/12) ([!5](https://gitlab.com/vekunz/markdown4documentation/merge_requests/5))

## v0.3.1

* Feature: Make PdfOptions available for file and directory mode [#7](https://gitlab.com/vekunz/markdown4documentation/issues/7) ([!3](https://gitlab.com/vekunz/markdown4documentation/merge_requests/3))

## v0.3.0

* Feature: Insert PDF pages in merge configuration [#5](https://gitlab.com/vekunz/markdown4documentation/issues/5) ([!1](https://gitlab.com/vekunz/markdown4documentation/merge_requests/1))
* Feature: Set pdf options in configuration and set header/footer of pdf [#6](https://gitlab.com/vekunz/markdown4documentation/issues/6) ([!2](https://gitlab.com/vekunz/markdown4documentation/merge_requests/22))

## v0.2.0

* Bug fixes
