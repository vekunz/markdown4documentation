#!/usr/bin/env node

require('./src/prototypes');

const [, , ...args] = process.argv;

const CliOptions = require('./src/cliOptions');
let options;
try {
    options = new CliOptions(args);
} catch (e) {
    console.error(e);
    process.exit(1);
}

const Processor = require('./src/processor');

const processor = new Processor(options.Configuration);
processor.Process().then();
