const CliOptions = jest.requireActual('../src/cliOptions');

describe('Create CliOptions()', () => {
    test('Error on no parameter', () => {
        expect(() => {
            new CliOptions();
        }).toThrowError('Required parameters missing');
    });

    test('Error on empty parameter', () => {
        expect(() => {
            new CliOptions([]);
        }).toThrowError('Required parameters missing');
    });
});

describe('Create CliOptions with parameters', () => {
    beforeEach(() => {
        jest.resetAllMocks();
        jest.resetModules();
        jest.mock('fs', () => {
            return {
                existsSync: (file) => { return true; },
                lstatSync: (file) => { return { isFile: () => true } }
            }
        });
        jest.mock('../src/configuration', () => jest.requireActual('../src/configuration'));
        jest.mock('path', () => {
            return {
                dirname: (file) => file,
                resolve: (file) => file
            }
        });
    });

    describe('Error on missing parameter', () => {
        test('--file missing', () => {
            expect(() => {
                new CliOptions(['--output', 'something']);
            }).toThrowError('Required parameter --file missing');
        });

        test('--output missing', () => {
            expect(() => {
                new CliOptions(['--file', 'something']);
            }).toThrowError('Required parameter --output missing');
        });

        test('--file no value', () => {
            expect(() => {
                new CliOptions(['--output', 'something', '--file']);
            }).toThrowError('Missing value for parameter file');
        });

        test('--output no value', () => {
            expect(() => {
                new CliOptions(['--file', 'something', '--output']);
            }).toThrowError('Missing value for parameter output');
        });

        test('--template no value', () => {
            expect(() => {
                new CliOptions(['--output', 'something', '--file', 'something', '--template']);
            }).toThrowError('Missing value for parameter template');
        });

        test('--type no value', () => {
            expect(() => {
                new CliOptions(['--output', 'something', '--file', 'something', '--type']);
            }).toThrowError('Missing value for parameter type');
        });

        test('--configuration no value', () => {
            expect(() => {
                new CliOptions(['--configuration']);
            }).toThrowError('Missing value for parameter configuration');
        });
    });

    describe('Invalid parameter', () => {
        test('Unknown parameter', () => {
            expect(() => {
                new CliOptions(['--output', 'something', '--unknown', 'something', '--type', 'invalid']);
            }).toThrowError(`Unknown parameter '--unknown'`);
        });

        test('Invalid --type', () => {
            expect(() => {
                new CliOptions(['--output', 'something', '--file', 'something', '--type', 'invalid']);
            }).toThrowError(`Unknown type 'invalid'`);
        });

        test('--file not exists', () => {
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => false
                }
            });

            expect(() => {
                new CliOptions(['--output', 'something', '--file', 'something']);
            }).toThrowError(`'something' is not a file`);
        });

        test('--file is not a file', () => {
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => true,
                    lstatSync: (file) => { return { isFile: () => false } }
                }
            });

            expect(() => {
                new CliOptions(['--output', 'something', '--file', 'something']);
            }).toThrowError(`'something' is not a file`);
        });

        test('--configuration is not a file', () => {
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => true,
                    lstatSync: (file) => { return { isFile: () => false } }
                }
            });

            expect(() => {
                new CliOptions(['--configuration', 'something']);
            }).toThrowError(`'something' is not a file`);
        });

        describe('--configuration with other parameters', () => {
            beforeEach(() => {
                const mockConfigFile = `
                {"jobs": [{
                    "input": "file.md",
                    "output": "file.html"
                }]}`;
                jest.unmock('fs');
                jest.mock('fs', () => {
                    return {
                        existsSync: (file) => { return true; },
                        lstatSync: (file) => { return { isFile: () => true } },
                        readFileSync: (file, options) => { return mockConfigFile }
                    }
                });
                jest.mock('../templates/default', () => null);
            });

            test('with --file', () => {
                expect(() => {
                    new CliOptions(['--configuration', 'something', '--file', 'something']);
                }).toThrowError('If configuration is set then no other parameter is allowed');
            });

            test('with --output', () => {
                expect(() => {
                    new CliOptions(['--configuration', 'something', '--output', 'something']);
                }).toThrowError('If configuration is set then no other parameter is allowed');
            });

            test('with --type', () => {
                expect(() => {
                    new CliOptions(['--configuration', 'something', '--type', 'html']);
                }).toThrowError('If configuration is set then no other parameter is allowed');
            });

            test('with --template', () => {
                expect(() => {
                    new CliOptions(['--configuration', 'something', '--template', 'default']);
                }).toThrowError('If configuration is set then no other parameter is allowed');
            });
        });

        describe('--template', () => {
            test('Custom template not a template', () => {
                jest.mock('path', () => { return { resolve: (path) => path } });

                expect(() => {
                    new CliOptions(['--output', 'something', '--file', 'something', '--template', 'custom']);
                }).toThrowError(`Unknown template 'custom'`);
            });

            test('Custom template not exists', () => {
                jest.unmock('fs');
                jest.mock('fs', () => {
                    return {
                        existsSync: (file) => file !== 'custom',
                        lstatSync: (file) => { return { isFile: () => true } }
                    }
                });
                jest.mock('path', () => { return { resolve: (path) => path } });

                expect(() => {
                    new CliOptions(['--output', 'something', '--file', 'something', '--template', 'custom']);
                }).toThrowError(`Unknown template 'custom'`);
            });
        });
    });

    test('set default parameter', () => {
        const inputFile = 'inputFile';
        const outputFile = 'outputFile';

        const options = new CliOptions(['--file', inputFile, '--output', outputFile]);
        const configuration = options.Configuration;

        const jobs = configuration.Jobs;
        expect(jobs).toHaveLength(1);
        expect(jobs[0].Type).toEqual('html');
        expect(jobs[0].Output).toEqual(outputFile);
        expect(jobs[0].Template).toEqual(undefined);
        expect(jobs[0].Input).toHaveLength(1);
        const task = jobs[0].Input[0];
        expect(task.Type).toEqual('file');
        expect(task.File).toEqual(inputFile);
        expect(task.SuppressLeadingH1).toEqual(false);
        expect(task.IndentHeadlines).toEqual(0);
        expect(task.Sub).toHaveLength(0);
    });

    test('set all parameters', () => {
        const inputFile = 'inputFile';
        const outputFile = 'outputFile';
        const template = 'material';
        const type = 'pdf';

        jest.mock('../src/configuration', () => jest.requireActual('../src/configuration'));
        jest.mock('../templates/material', () => null);

        const options = new CliOptions(['--file', inputFile, '--output', outputFile, '--template', template, '--type', type]);
        const configuration = options.Configuration;

        const jobs = configuration.Jobs;
        expect(jobs).toHaveLength(1);
        expect(jobs[0].Type).toEqual('pdf');
        expect(jobs[0].Output).toEqual(outputFile);
        expect(jobs[0].Template).toEqual('material');
        expect(jobs[0].Input).toHaveLength(1);
        const task = jobs[0].Input[0];
        expect(task.Type).toEqual('file');
        expect(task.File).toEqual(inputFile);
        expect(task.SuppressLeadingH1).toEqual(false);
        expect(task.IndentHeadlines).toEqual(0);
        expect(task.Sub).toHaveLength(0);
    });

    test('Custom template', () => {
        jest.mock('path', () => { return { resolve: (path) => '../templates/material.js' } });
        jest.mock('../templates/material.js', () => null);

        expect(() => {
            new CliOptions(['--output', 'something', '--file', 'something', '--template', 'something']);
        }).not.toThrow();
    });

    describe('Configuration file', () => {
        test('mode file - success', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "mode": "file",
                    "type": "html",
                    "input": "file.md",
                    "output": "file.html",
                    "template": "default",
                    "suppressLeadingH1": true,
                    "headingNumbering": true
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();

            const jobs = configuration.Jobs;
            expect(jobs).toHaveLength(1);
            expect(jobs[0].Type).toEqual('html');
            expect(jobs[0].Output).toEqual('file.html');
            expect(jobs[0].Template).toEqual('default');
            expect(jobs[0].Input).toHaveLength(1);
            expect(jobs[0].HeadingNumbering).toBeTruthy();
            const task = jobs[0].Input[0];
            expect(task.Type).toEqual('file');
            expect(task.File).toEqual('file.md');
            expect(task.SuppressLeadingH1).toEqual(true);
            expect(task.IndentHeadlines).toEqual(0);
            expect(task.Sub).toHaveLength(0);
        });

        test('mode file - pdfOptions', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "input": "file.md",
                    "output": "file.html",
                    "pdfOptions": {
                        "format": "A5",
                        "width": "200mm",
                        "height": "300mm",
                        "margin": {
                            "top": "5cm",
                            "right": "2.5cm",
                            "bottom": "1.5cm",
                            "left": "1.7cm"
                        },
                        "displayHeaderFooter": true,
                        "landscape": true,
                        "headerTemplate": "header",
                        "footerTemplate": "footer"
                    }
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);

            const pdfOptions = configuration.Jobs[0].PdfOptions;
            expect(pdfOptions).toBeDefined();

            expect(pdfOptions.Format).toEqual('A5');
            expect(pdfOptions.Width).toEqual('200mm');
            expect(pdfOptions.Height).toEqual('300mm');
            expect(pdfOptions.Landscape).toBeTruthy();
            expect(pdfOptions.DisplayHeaderFooter).toBeTruthy();
            expect(pdfOptions.HeaderTemplate).toEqual('header');
            expect(pdfOptions.FooterTemplate).toEqual('footer');
            expect(pdfOptions.Margin).toBeDefined();
            expect(pdfOptions.Margin.Top).toEqual('5cm');
            expect(pdfOptions.Margin.Right).toEqual('2.5cm');
            expect(pdfOptions.Margin.Bottom).toEqual('1.5cm');
            expect(pdfOptions.Margin.Left).toEqual('1.7cm');
        });

        test('mode file - pdfOptions - empty templates', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "input": "file.md",
                    "output": "file.html",
                    "pdfOptions": {
                        "displayHeaderFooter": true,
                        "headerTemplate": ""
                    }
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);

            const pdfOptions = configuration.Jobs[0].PdfOptions;
            expect(pdfOptions).toBeDefined();

            expect(pdfOptions.HeaderTemplate).toEqual('<span></span>');
            expect(pdfOptions.FooterTemplate).toEqual('<span></span>');
        });

        test('mode file - pdfOptions - not format', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "input": "file.md",
                    "output": "file.html",
                    "pdfOptions": {
                        "width": "200mm",
                        "height": "300mm"
                    }
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);

            const pdfOptions = configuration.Jobs[0].PdfOptions;
            expect(pdfOptions).toBeDefined();

            expect(pdfOptions.Format).toBeUndefined();
            expect(pdfOptions.Width).toEqual('200mm');
            expect(pdfOptions.Height).toEqual('300mm');
        });

        test('mode directory - pdfOptions', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "mode": "directory",
                    "input": "./",
                    "output": "./",
                    "pdfOptions": {
                        "format": "A5",
                        "width": "200mm",
                        "height": "300mm",
                        "margin": {
                            "top": "5cm",
                            "right": "2.5cm",
                            "bottom": "1.5cm",
                            "left": "1.7cm"
                        },
                        "displayHeaderFooter": true,
                        "landscape": true,
                        "headerTemplate": "header",
                        "footerTemplate": "footer"
                    }
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return {
                        isDirectory: () => file === './',
                        isFile: () => file !== './'
                    } },
                    readFileSync: (file, options) => { return mockConfigFile },
                    readdirSync: (path) => {return  ['file.md']}
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);

            const pdfOptions = configuration.Jobs[0].PdfOptions;
            expect(pdfOptions).toBeDefined();

            expect(pdfOptions.Format).toEqual('A5');
            expect(pdfOptions.Width).toEqual('200mm');
            expect(pdfOptions.Height).toEqual('300mm');
            expect(pdfOptions.Landscape).toBeTruthy();
            expect(pdfOptions.DisplayHeaderFooter).toBeTruthy();
            expect(pdfOptions.HeaderTemplate).toEqual('header');
            expect(pdfOptions.FooterTemplate).toEqual('footer');
            expect(pdfOptions.Margin).toBeDefined();
            expect(pdfOptions.Margin.Top).toEqual('5cm');
            expect(pdfOptions.Margin.Right).toEqual('2.5cm');
            expect(pdfOptions.Margin.Bottom).toEqual('1.5cm');
            expect(pdfOptions.Margin.Left).toEqual('1.7cm');
        });

        test('mode merge - pdfOptions', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "mode": "merge",
                    "input": [
                        {
                            "file": "file.md"
                        }
                    ],
                    "output": "file.html",
                    "pdfOptions": {
                        "format": "A5",
                        "width": "200mm",
                        "height": "300mm",
                        "margin": {
                            "top": "5cm",
                            "right": "2.5cm",
                            "bottom": "1.5cm",
                            "left": "1.7cm"
                        },
                        "displayHeaderFooter": true,
                        "landscape": true,
                        "headerTemplate": "header",
                        "footerTemplate": "footer"
                    }
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);

            const pdfOptions = configuration.Jobs[0].PdfOptions;
            expect(pdfOptions).toBeDefined();

            expect(pdfOptions.Format).toEqual('A5');
            expect(pdfOptions.Width).toEqual('200mm');
            expect(pdfOptions.Height).toEqual('300mm');
            expect(pdfOptions.Landscape).toBeTruthy();
            expect(pdfOptions.DisplayHeaderFooter).toBeTruthy();
            expect(pdfOptions.HeaderTemplate).toEqual('header');
            expect(pdfOptions.FooterTemplate).toEqual('footer');
            expect(pdfOptions.Margin).toBeDefined();
            expect(pdfOptions.Margin.Top).toEqual('5cm');
            expect(pdfOptions.Margin.Right).toEqual('2.5cm');
            expect(pdfOptions.Margin.Bottom).toEqual('1.5cm');
            expect(pdfOptions.Margin.Left).toEqual('1.7cm');
        });

        test('mode merge - task type heading', () => {
            const mockConfigFile = `
                {"jobs": [{
                    "mode": "merge",
                    "input": [
                        {
                            "type": "heading",
                            "content": "My Heading"
                        }
                    ],
                    "output": "file.html"
                }]}`;
            jest.unmock('fs');
            jest.mock('fs', () => {
                return {
                    existsSync: (file) => { return true; },
                    lstatSync: (file) => { return { isFile: () => true } },
                    readFileSync: (file, options) => { return mockConfigFile }
                }
            });
            jest.mock('../templates/default', () => null);

            const options = new CliOptions(['--configuration', 'config.json']);
            const configuration = options.Configuration;
            expect(configuration).toBeDefined();
            expect(configuration.Jobs).toHaveLength(1);
            expect(configuration.Jobs[0].Input).toHaveLength(1);

            const task = configuration.Jobs[0].Input[0];
            expect(task.Type).toEqual('heading');
            expect(task.Content).toEqual('My Heading');
        });
    });
});
