﻿MATERIAL_TEMPLATE = {
  html: `<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
  <style type="text/css">
    .custom-blockquote {
      color: grey;
      border-left: 0.3em solid grey;
      padding-left: 1em;
      margin-left: 1em;
    }
    .custom-pre {
      background-color: lightgrey;
      padding: 0.5em;
      margin: 0.5em;
    }
    .custom-code {
      font-size: 1rem;
      background-color: lightgrey;
    }
  </style>
</head>
<body class="mdc-typography">
\${CONTENT}
</body>
</html>
`,

  h1: '<h1 class="mdc-typography--headline1"${ATTRIBUTES}>${CONTENT}</h1>',
  h2: '<h2 class="mdc-typography--headline2"${ATTRIBUTES}>${CONTENT}</h2>',
  h3: '<h3 class="mdc-typography--headline3"${ATTRIBUTES}>${CONTENT}</h3>',
  h4: '<h4 class="mdc-typography--headline4"${ATTRIBUTES}>${CONTENT}</h4>',
  h5: '<h5 class="mdc-typography--headline5"${ATTRIBUTES}>${CONTENT}</h5>',
  h6: '<h6 class="mdc-typography--headline6"${ATTRIBUTES}>${CONTENT}</h6>',

  p: '<p class="mdc-typography--body1">${CONTENT}</p>',

  blockquote: '<blockquote class="custom-blockquote">${CONTENT}</blockquote>',

  pre: '<pre class="custom-pre mdc-elevation--z3">${CONTENT}</pre>',
  code: '<code class="custom-code">${CONTENT}</code>',

  ul: '<ul class="mdc-typography--body1">${CONTENT}</ul>',
  ol: '<ol class="mdc-typography--body1">${CONTENT}</ol>',
  li: '<li>${CONTENT}</li>',

  em: '<em>${CONTENT}</em>',
  strong: '<strong>${CONTENT}</strong>',

  hr: '<hr />',

  a: '<a${ATTRIBUTES}>${CONTENT}</a>',
  img: '<img${ATTRIBUTES} />',

  hNumbering: {
    level1: '${l1}. ',
    level2: '${l1}.${l2}. ',
    level3: '${l1}.${l2}.${l3}. ',
    level4: '${l1}.${l2}.${l3}.${l4}. ',
    level5: '${l1}.${l2}.${l3}.${l4}.${l5}. ',
    level6: '${l1}.${l2}.${l3}.${l4}.${l5}.${l6}. '
  },
};

module.exports = MATERIAL_TEMPLATE;
