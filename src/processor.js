class Processor {
    fs = require('fs');
    HtmlParser = require('./htmlParser');
    HtmlBuilder = require('./htmlBuilder');

    #configuration;
    #htmlBuilder = {};

    constructor(configuration) {
        this.#configuration = configuration;
    }

    async Process() {
        for (const job of this.#configuration.Jobs) {
            await this.#ExecuteJob(job);
        }
    }

    #ExecuteJob = async (job) => {
        if (job.Type === '{copy}') {
            const promise = new Promise((resolve, reject) => {
                const ncp = require('ncp').ncp;
                ncp(job.Input, job.Output, (err) => {
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            });
            await promise;
            return;
        } else if (job.Type === '{delete}') {
            const promise = new Promise((resolve) => {
                const rimraf = require('rimraf');
                rimraf(job.Input, () => resolve());
            });
            await promise;
            return;
        }

        let htmlBuilder;
        if (Object.keys(this.#htmlBuilder).includes(job.Template)) {
            htmlBuilder = this.#htmlBuilder[job.Template];
        }
        else {
            htmlBuilder = new this.HtmlBuilder(this.#GetTemplate(job.Template));
            this.#htmlBuilder[job.Template] = htmlBuilder;
        }

        htmlBuilder.ResetHNumbers();
        htmlBuilder.EnableHNumbering(job.HeadingNumbering);

        let parts = [];
        let result = '';
        for (const task of job.Input) {
            if (task.Type === 'file' && task.File.endsWith('.pdf')) {
                if (job.Type !== 'pdf')
                    throw 'PDF files are not supported with job type html';
                if (result !== '') {
                    const page = htmlBuilder.BuildPage(result);
                    parts.push({type: 'html', file: page});
                    result = '';
                }
                parts.push({type: 'pdf', file: task.File});
            } else {
                result += this.#ProcessTask(task, htmlBuilder, 1);
            }
        }
        if (result !== '') {
            const page = htmlBuilder.BuildPage(result);
            parts.push({type: 'html', file: page});
        } else if (parts.length === 0) {
            parts.push('');
        }

        if (job.Type === 'html') {
            this.#WriteFile(job.Output, parts[0].file);
        } else if (job.Type === 'pdf') {
            const PDFDocument = require('pdf-lib').PDFDocument;
            let files = [];
            let counter = 0;
            for (const part of parts) {
                if (part.type === 'pdf') {
                    const binary = this.fs.readFileSync(part.file);
                    const pdf = await PDFDocument.load(binary);
                    files.push(pdf);
                } else {
                    counter++;
                    let pdfFile = `${job.Output}.${counter}.html`;
                    this.#WriteFile(pdfFile, part.file);
                    await this.#ConvertToPdf(pdfFile, job.PdfOptions);
                    const binary = this.fs.readFileSync(pdfFile);
                    const pdf = await PDFDocument.load(binary);
                    files.push(pdf);
                    this.fs.unlinkSync(pdfFile);
                }
            }

            const pdfDoc = await PDFDocument.create();
            for (const file of files) {
                const indices = [];
                for (let i = 0; i < file.getPageCount(); i++)
                    indices.push(i);
                const pages = await pdfDoc.copyPages(file, indices);

                for (const page of pages) {
                    pdfDoc.addPage(page);
                }
            }

            pdfDoc.setCreator('markdown4documentation - https://gitlab.com/vekunz/markdown4documentation');
            pdfDoc.setProducer('markdown4documentation - https://gitlab.com/vekunz/markdown4documentation');

            const pdfBytes = await pdfDoc.save();
            this.fs.writeFileSync(job.Output, pdfBytes);
        }
    };

    #ProcessTask = (task, htmlBuilder, level) => {
        let html;
        if (task.type === 'file' && (task.File.endsWith('.html') || task.File.endsWith('.htm'))) {
            html = this.#ReadFile(task.File);
        } else {
            let fileContent = '';
            if (task.Type === 'file') {
                fileContent = this.#ReadFile(task.File);
            } else if (task.Type === 'heading') {
                fileContent = `# ${task.Content}`;
            }
            const htmlParser = new this.HtmlParser(fileContent, htmlBuilder);
            htmlParser.BaseHeadlineLevel = level + task.IndentHeadlines;
            htmlParser.SuppressLeadingH1 = task.SuppressLeadingH1;
            html = htmlParser.Parse() + '\r\n';
        }

        for (const subTask of task.Sub) {
            html += this.#ProcessTask(subTask, htmlBuilder, level + 1 + task.IndentHeadlines);
        }

        return html;
    };

    #GetTemplate = (template) => {
        if (template) {
            try {
                return require('../templates/' + template);
            } catch {
                const path = require('path');
                template = path.resolve(template);
                if (this.fs.existsSync(template) && this.fs.lstatSync(template).isFile()) {
                    try {
                        return require(template);
                    } catch (e) {
                        console.error(`Failed to load template '${template}'`);
                        console.log(e);
                        process.exit(1);
                    }
                } else {
                    console.error(`Unknown template '${template}'`);
                    process.exit(1);
                }

            }
        } else {
            return require('../templates/default');
        }
    };

    #ReadFile = (file) => {
        const bytes = [...this.fs.readFileSync(file)].trimBom();
        const buffer = Buffer.from(bytes);
        return buffer.toString('utf-8');
    };

    #WriteFile = (file, content) => {
        this.fs.writeFileSync(file, content, {encoding: 'utf-8'});
    };

    #ConvertToPdf = async (file, options) => {
        const puppeteer = require('puppeteer');
        const path = require('path');

        const browser = await puppeteer.launch({
            headless: true,
            args: ["--no-sandbox", "--disable-setuid-sandbox", "--allow-file-access-from-files"]
        });
        try {
            const page = await browser.newPage();
            await page.setExtraHTTPHeaders({ContentType: 'text/html'});
            const url = 'file://' + path.resolve(file);
            await page.goto(url);
            await page.pdf({
                path: path.resolve(file),
                printBackground: true,
                format: options.Format,
                width: options.Width,
                height: options.Height,
                margin: {
                    top: options.Margin.Top,
                    right: options.Margin.Right,
                    bottom: options.Margin.Bottom,
                    left: options.Margin.Left
                },
                displayHeaderFooter: options.DisplayHeaderFooter,
                landscape: options.Landscape,
                headerTemplate: options.HeaderTemplate,
                footerTemplate: options.FooterTemplate
            });

            await browser.close();
        } catch (e) {
            console.error(e);
            process.exit(1);
        }
    };
}

module.exports = Processor;
