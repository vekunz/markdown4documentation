class Configuration {

    #jobs = [];

    CreateJob() {
        const job = new Job();
        job.PdfOptions = new PdfOptions();
        this.#jobs.push(job);
        return job;
    }

    get Jobs() {
        return this.#jobs;
    }

}

module.exports = Configuration;

class Job {
    Type = 'html';
    Input = [];
    Output;
    Template;
    PdfOptions;
    HeadingNumbering = false;

    CreateTask() {
        const task = new Task();
        this.Input.push(task);
        return task;
    }
}

class PdfOptions {
    Format = 'A4';
    Width;
    Height;
    Margin = {
        Top: "1cm",
        Right: "1.5cm",
        Bottom: "1cm",
        Left: "1.5cm"
    };
    DisplayHeaderFooter = false;
    Landscape = false;
    HeaderTemplate;
    FooterTemplate;
}

class Task {
    Type = 'file';
    File;
    Content;
    SuppressLeadingH1 = false;
    IndentHeadlines = 0;
    Sub = [];

    CreateSubTask() {
        const task = new Task();
        this.Sub.push(task);
        return task;
    }
}
