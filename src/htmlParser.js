require('./prototypes');

class HtmlParser {

    #parser;
    #options = new Options();
    #markdown = '';
    #builder;

    constructor(markdown, builder) {
        if (markdown)
            this.#markdown = markdown;
        if (builder)
            this.#builder = builder;
        else {
            const HtmlBuilder = require('./htmlBuilder');
            this.#builder = new HtmlBuilder();
        }
        this.#parser = new HtmlParserInternal(this.#markdown, this.#builder);
    }

    Parse() {
        this.#parser.Options = this.#options;
        return this.#parser.Parse();
    }

    set BaseHeadlineLevel(value) {
        let intValue = Number.parseInt(value);
        if (intValue < 1 || intValue > 6)
            throw 'Invalid BaseHeadlineLevel, only values from 1 to 6 are allowed';
        this.#options.BaseHeadlineLevel = intValue;
    }

    set SuppressLeadingH1(value) {
        this.#options.SuppressLeadingH1 = value;
    }

}

module.exports = HtmlParser;

class Options {
    BaseHeadlineLevel = 1;
    SuppressLeadingH1 = false;
}

class HtmlParserInternal {
    options = new Options();
    ignoreTagIfSingleP = false;
    markdown;
    builder;
    references = {};

    constructor(markdown, builder) {
        this.markdown = markdown;
        this.builder = builder;
    }

    set Options(value) {
        this.options = value;
    }

    set IgnoreTagIfSingleP(value) {
        this.ignoreTagIfSingleP = value;
    }

    closedBlocks = [];

    blockOpen = false;
    currentBlock;

    Parse() {
        let lines = this.markdown.split(/\r?\n/g);
        if (lines.last().trim() !== '')
            lines.push('');
        lines.push('');
        for (let line of lines) {
            line = this.Mask(line);

            if (this.blockOpen && line.trim() === '' && this.currentBlock.Type !== 'ul' && this.currentBlock.Type !== 'ol' && this.currentBlock.Type !== 'pre' && this.currentBlock.Type !== 'html') {
                this.CloseBlock();
                continue;
            } else if (!this.blockOpen && line.trim() === '')
                continue;

            if (this.CheckCodeFences(line))
                continue;

            if (this.CheckReference(line, true))
                continue;

            if (this.CheckHr(line))
                continue;

            if (this.CheckList('ul', line))
                continue;

            if (this.CheckList('ol', line))
                continue;

            if (this.CheckCodeIndents(line))
                continue;

            if (this.CheckHeader(line))
                continue;

            if (this.CheckBlockquote(line))
                continue;

            if (this.CheckHtml(line))
                continue;

            if (this.CheckReference(line, false))
                continue;

            if (!this.blockOpen && line.trim() !== '')
                this.NewBlock();
            if (this.blockOpen)
                this.currentBlock.Content.push(line);
        }
        this.CloseBlock();

        let result = '';
        if (this.closedBlocks.length === 1 && this.closedBlocks[0].Type === 'p' && this.ignoreTagIfSingleP) {
            let block = this.closedBlocks[0];
            block.Option = {ignoreSurroundingTag: true};
            result += block;
        } else {
            if (this.ignoreTagIfSingleP && this.closedBlocks.length === 2 && this.closedBlocks[0].Type === 'p' && this.closedBlocks[1].Type === 'ul')
                this.closedBlocks[0].Option = {ignoreSurroundingTag: true};

            for (let block of this.closedBlocks) {
                result += block + '\r\n';
            }
        }
        return result;
    }

    Mask(text) {
        const mask = {
            '\\\\': '&#92;',
            '\\`': '&#96;',
            '\\*': '&#42;',
            '\\_': '&#95;',
            '\\{': '&#123;',
            '\\}': '&#125;',
            '\\[': '&#91;',
            '\\]': '&#93;',
            '\\(': '&#40;',
            '\\)': '&#41;',
            '\\#': '&#35;',
            '\\+': '&#43;',
            '\\-': '&#45;',
            '\\.': '&#46;',
            '\\!': '&#33;',
            '\\<': '&lt;',
        };
        for (const entry of Object.entries(mask))
            text = text.replace(entry[0], entry[1]);

        return text;
    }

    CheckHr(line) {
        if ((line.trimStartMax(3).startsWith('*') || line.trimStartMax(3).startsWith('-'))
            && (line.stripAll(' ').consistsOf('*') || line.stripAll(' ').consistsOf('-'))) {
            if (this.blockOpen && line.trimStartMax(3).trimEnd().consistsOf('-') && this.currentBlock.Content.length === 1 && this.currentBlock.Type === '')
                return false;
            this.NewBlock();
            this.currentBlock.Type = 'hr';
            this.CloseBlock();
            return true;
        }
        return false;
    }

    CheckList(type, line) {
        if (this.blockOpen && this.currentBlock.Type === 'html')
            return false;
        if (line.trimStartMax(3).startsWithListElement(type) && (!this.blockOpen || this.currentBlock.Type !== type || (type === 'ul' && this.currentBlock.Option !== line.trim()[0]))) {
            this.NewBlock();
            this.currentBlock.Type = type;
            if (type === 'ul')
                this.currentBlock.Option = line.trim().substr(0, 1);
        }

        if (line.trimStartMax(3).startsWithListElement(type)) {
            let li = new Block(this.builder, this.options, this.references);
            li.Type = 'li';
            li.Content.push(line.trim().trimListElement(type));
            this.currentBlock.Content.push(li);
            return true;
        } else if (this.blockOpen && this.currentBlock.Type === type) {
            const lastLi = this.currentBlock.Content.last().Content.last();
            if (lastLi.trim() !== '') {
                this.currentBlock.Content.last().Content.push(line.trimStartMax(4));
            } else if (line.startsWith('\t')) {
                this.currentBlock.Content.last().Content.push(line.substr(1));
            } else if (line.startsWith('    ')) {
                this.currentBlock.Content.last().Content.push(line.substr(4));
            } else {
                let forceParse = false;
                for (let i = 0; i < this.currentBlock.Content.length; i++) {
                    let elem = this.currentBlock.Content[i];
                    for (let j = 0; j < elem.Content.length; j++) {
                        if (elem.Content[j] === '' && !((i + 1) === this.currentBlock.Content.length && (j + 1) === elem.Content.length)) {
                            forceParse = true;
                            break;
                        }
                        if (forceParse) {
                            break;
                        }
                    }
                }
                if (forceParse)
                    for (let li of this.currentBlock.Content)
                        li.Option = {forceParse: true};
                this.CloseBlock();
                return false;
            }
            return true;
        }
        return false;

    }

    CheckHeader(line) {
        let checkLevel = function (line, level) {
            if (line.length > level && line.trim().substr(0, level).consistsOf('#')) {
                this.NewBlock();
                this.currentBlock.Type = this.CalcH(level);
                this.currentBlock.Content.push(line.trim().trimChar('#').trim());
                this.currentBlock.Option = {
                    attributes: {
                        id: this.currentBlock.Content[0].toLowerCase().replace(/[^\w\- ]+/g, '').replace(/\s/g, '-').replace(/\-+$/, '')
                    }
                };
                this.CloseBlock();
                return true;
            }
            return false;
        }.bind(this);

        if (this.blockOpen && this.currentBlock.Type === '' && this.currentBlock.Content.length === 1 && line.trimStartMax(3).trimEnd().consistsOf('=')) {
            this.currentBlock.Type = this.CalcH(1);
            this.currentBlock.Option = {
                attributes: {
                    id: this.currentBlock.Content[0].toLowerCase().replace(/[^\w\- ]+/g, '').replace(/\s/g, '-').replace(/\-+$/, '')
                }
            };
            this.CloseBlock();
            return true;
        } else if (this.blockOpen && this.currentBlock.Type === '' && this.currentBlock.Content.length === 1 && line.trimStartMax(3).trimEnd().consistsOf('-')) {
            this.currentBlock.Type = this.CalcH(2);
            this.currentBlock.Option = {
                attributes: {
                    id: this.currentBlock.Content[0].toLowerCase().replace(/[^\w\- ]+/g, '').replace(/\s/g, '-').replace(/\-+$/, '')
                }
            };
            this.CloseBlock();
            return true;
        } else if (checkLevel(line, 6) || checkLevel(line, 5) || checkLevel(line, 4) || checkLevel(line, 3) || checkLevel(line, 2) || checkLevel(line, 1)) {
            return true;
        }
        return false;
    }

    CheckBlockquote(line) {
        if (this.blockOpen && this.currentBlock.Type === 'html')
            return false;
        if (line.trim()[0] === '>' || (this.blockOpen && this.currentBlock.Type === 'blockquote')) {
            if (!this.blockOpen || this.currentBlock.Type !== 'blockquote') {
                this.NewBlock();
                this.currentBlock.Type = 'blockquote';
            }
            if (line.trim()[0] === '>')
                line = line.trim().substr(1).trimStartMax(1);
            else
                line = line.trim();
            this.currentBlock.Content.push(line);
            return true;
        }
        return false;
    }

    CheckCodeFences(line) {
        if ((!this.blockOpen || this.currentBlock.Type !== 'pre' || !this.currentBlock.Option.fenced ) && line.trimStartMax(3).startsWith('```')) {
            this.NewBlock();
            this.currentBlock.Type = 'pre';
            this.currentBlock.Option = {fenced: true};
            let code = new Block(this.builder, this.options, this.references);
            code.Type = 'code';
            this.currentBlock.Content.push(code);
            return true;
        }
        if (this.blockOpen && this.currentBlock.Type === 'pre' && this.currentBlock.Option.fenced) {
            if (line.trimStartMax(3).trimEnd() === '```') {
                this.CloseBlock();
                return true;
            }

            this.currentBlock.Content[0].Content.push(line);
            return true;
        }
        return false;
    }

    CheckCodeIndents(line) {
        let codeLine = (line) => (line.startsWith('\t') && line.length > 1) || (line.startsWith('    ') && line.length > 4);
        let openCodeBlock = () => this.blockOpen && this.currentBlock.Type === 'pre';

        if (this.blockOpen && (this.currentBlock.Type !== 'pre' || this.currentBlock.Option.fenced))
            return false;

        if (openCodeBlock() && line.trim() !== '' && !codeLine(line)) {
            this.CloseBlock();
            return false;
        }

        if (!this.blockOpen && codeLine(line)) {
            this.NewBlock();
            this.currentBlock.Type = 'pre';
            this.currentBlock.Option = {fenced: false};
            let code = new Block(this.builder, this.options, this.references);
            code.Type = 'code';
            this.currentBlock.Content.push(code);
            this.currentBlock.Content[0].Content.push(line.substr(4));
        } else if (openCodeBlock() && line === '') {
            this.currentBlock.Content[0].Content.push(line);
        } else if (openCodeBlock()) {
            this.currentBlock.Content[0].Content.push(line.substr(4));
        } else {
            return false;
        }
        return true;
    }

    CheckHtml(line) {
        if (!this.blockOpen || this.currentBlock.Type !== 'html') {
            if (line.trim().length < 2 || line.trim()[0] !== '<' || line.trim().substr(0, 2) === '<>')
                return false;

            const indexLt = line.indexOf('<');
            const indexGt = line.indexOf('>', indexLt);
            const indexSp = line.indexOf(' ', indexLt);

            let tag = '';
            if (indexGt === -1 && indexSp === -1)
                tag = line.substr(indexLt + 1);
            else if (indexSp === -1 || (indexGt !== -1 && indexSp > indexGt))
                tag = line.substring(indexLt + 1, indexGt);
            else
                tag = line.substring(indexLt + 1, indexSp);


            if (tag.isURL() || tag.isEmail() || tag.match(/^[a-zA-Z]+$/) === null)
                return false;

            this.NewBlock();
            this.currentBlock.Type = 'html';

            this.currentBlock.Option = {openCount: 0, tag: tag};
        }
        if (this.currentBlock.Option['searchClosingGt']) {
            this.currentBlock.Content.push(line);
            if (line.indexOf('>') !== -1)
                this.CloseBlock();
            return true;
        }

        const tag = this.currentBlock.Option.tag;
        const opCount = (line.match(new RegExp(`<${tag}`, 'g')) || []).length;
        const clCount = (line.match(new RegExp(`</${tag}`, 'g')) || []).length;

        this.currentBlock.Option.openCount = this.currentBlock.Option.openCount + opCount - clCount;

        this.currentBlock.Content.push(line);

        if (this.currentBlock.Option.openCount === 0) {
            const closingTag = `</${tag}`;
            let closingIndex = line.lastIndexOf(closingTag);
            const closingGt = line.indexOf('>', closingIndex);

            if (closingGt === -1) {
                this.currentBlock.Option['searchClosingGt'] = true;
            } else {
                this.CloseBlock();
            }
        }

        return true;
    }

    CheckReference(line, onlyTitle) {
        if ((this.blockOpen && this.currentBlock.Type !== '{reference}') || line.trim().length < 2)
            return false;

        if (this.blockOpen && this.currentBlock.Type === '{reference}') {
            const rest = line.trim();
            if ((rest[0] === '"' && rest[rest.length - 1] === '"') || (rest[0] === '\'' && rest[rest.length - 1] === '\'') || (rest[0] === '(' && rest[rest.length - 1] === ')')) {
                const title = rest.substring(1, rest.length - 1);
                const refId = this.currentBlock.Option['refId'];
                this.references[refId][1] = title;
                this.CloseBlock();
                return true;
            }
            this.CloseBlock();
            return false;
        }

        if (onlyTitle)
            return false;

        const openingBracket = line.indexOf('[');
        const closingBracket = line.indexOf(']');

        if (openingBracket === -1 || closingBracket === -1 || line[closingBracket + 1] !== ':')
            return false;

        const refId = line.substring(openingBracket + 1, closingBracket);

        if (refId.trim() === '' || Object.keys(this.references).includes(refId))
            return false;

        const rest = line.substring(closingBracket + 2).trim();
        if (rest === '')
            return false;

        let url;
        let title;

        const space = rest.indexOf(' ');
        if (space === -1) {
            if (rest[0] === '<' && rest[rest.length - 1] === '>')
                url = rest.substring(1, rest.length - 1);
            else
                url = rest;
        } else {
            if (rest[0] === '<' && rest[space - 1] === '>')
                url = rest.substring(1, space - 1);
            else
                url = rest.substring(0, space);
            title = rest.substring(space + 1);
            if (title.length < 2)
                return false;
            if ((title[0] === '"' && title[title.length - 1] === '"') || (title[0] === '\'' && title[title.length - 1] === '\'') || (title[0] === '(' && title[title.length - 1] === ')')) {
                title = title.substring(1, title.length - 1);
            } else
                return false;
        }

        if (!title) {
            this.NewBlock();
            this.currentBlock.Type = '{reference}';
            this.currentBlock.Option = {refId: refId};
        }
        this.references[refId] = [url, title];

        return true;
    }

    NewBlock() {
        this.CloseBlock();
        this.currentBlock = new Block(this.builder, this.options, this.references);
        this.blockOpen = true;
    }

    CloseBlock() {
        if (this.blockOpen) {
            if (this.currentBlock.Type === '{reference}') {
                this.currentBlock = null;
                this.blockOpen = false;
                return;
            }
            if (this.options.SuppressLeadingH1 && this.currentBlock.Type === this.CalcH(1) && this.closedBlocks.length === 0) {
                this.currentBlock = null;
                this.blockOpen = false;
                return;
            }
            if (this.currentBlock.Type === '')
                this.currentBlock.Type = 'p';
            if (this.currentBlock.Type === 'p') {
                for (let i = 0; i < this.currentBlock.Content.length; i++) {
                    let line = this.currentBlock.Content[i];
                    if (i + 1 < this.currentBlock.Content.length && line.endsWith('  ')) {
                        line = line.substr(0, line.length - 2) + '<br />';
                        this.currentBlock.Content[i] = line;
                    }
                }
            }
            if (this.currentBlock.Type !== 'p' || this.currentBlock.Content.removeEmptyEntries().length > 0)
                this.closedBlocks.push(this.currentBlock);
            this.currentBlock = null;
            this.blockOpen = false;
        }
    }

    CalcH(level) {
        level += this.options.BaseHeadlineLevel - 1;
        if (level > 6)
            level = 6;
        return 'h' + level;
    }
}

class InlineParser {
    text = '';
    builder;
    references = {};

    constructor(text, builder, references) {
        if (text)
            this.text = text;
        this.builder = builder;
        if (references)
            this.references = references;
    }

    Parse() {
        if (this.text.trim().length === 0)
            return this.text;

        this.text = this.EscapeSingleLt(this.text);

        this.text = this.CheckCode(this.text);

        this.text = this.CheckLink(this.text);

        this.text = this.Check(this.text, '*', 'em');
        this.text = this.Check(this.text, '_', 'em');

        this.text = this.Check(this.text, '**', 'strong');
        this.text = this.Check(this.text, '__', 'strong');

        return this.text;
    }

    EscapeSingleLt(text) {
        let escapeIndices = [];

        let foundIndex = -1;
        let inTagName = false;
        let inAttr;
        for (let i = 0; i < text.length; i++) {
            const char = text[i];
            if (foundIndex === -1) {
                if (i + 1 === text.length)
                    break;
                else if (char === '<') {
                    if (text[i + 1] === ' ' || (text[i + 1] === '/' && text[i + 2] === ' ')) {
                        escapeIndices.push(i);
                    } else {
                        foundIndex = i;
                        inTagName = true;
                    }
                }
            } else {
                if (inTagName) {
                    if (char === '>') {
                        foundIndex = -1;
                        inTagName = false;
                    } else if (char === ' ') {
                        inTagName = false;
                    } else if (char === '<') {
                        escapeIndices.push(foundIndex);
                        foundIndex = i;
                    }
                } else {
                    if (inAttr) {
                        if (char === inAttr && text[i - 1] !== '\\') {
                            inAttr = undefined;
                        }
                    } else {
                        if ((char === '"' || char === '\'') && text[i - 1] !== '\\') {
                            inAttr = char;
                        } else if (char === '>') {
                            if (text[i - 1] === '=')
                                escapeIndices.push(foundIndex);
                            foundIndex = -1;
                        } else if (char === '<') {
                            escapeIndices.push(foundIndex);
                            foundIndex = i;
                            inTagName = true;
                        }
                    }
                }

            }
        }
        if (foundIndex !== -1)
            escapeIndices.push(foundIndex);

        for (let i of escapeIndices.reverse()) {
            text = text.substring(0, i) + '&lt;' + text.substring(i + 1);
        }

        return text;
    }

    CheckLink(text) {
        let links = [];

        let watch = 0;
        while (true) {
            const openName = text.indexOf('[', watch);
            if (openName === -1)
                break;
            const closeName = text.indexOf(']', openName + 1);
            if (closeName === -1)
                break;

            const linkName = text.substring(openName + 1, closeName);
            const isImg = (openName > 0 && text[openName - 1] === '!');

            const isRef = (text[closeName + 1] === '[' || (text[closeName + 1] === ' ' && text[closeName + 2] === '['));
            if (!isRef && text[closeName + 1] !== '(') {
                watch = closeName + 1;
                continue;
            }

            if (isRef) {
                const openId = text.indexOf('[', closeName);
                const closeId = text.indexOf(']', openId + 1);
                if (closeId === -1) {
                    break;
                }
                let id = text.substring(openId + 1, closeId);
                if (id === '')
                    id = linkName;

                if (Object.keys(this.references).includes(id)) {
                    links.push({
                        startIndex: (isImg ? openName - 1 : openName),
                        endIndex: closeId,
                        isImg: isImg,
                        name: linkName,
                        url: this.references[id][0],
                        title: this.references[id][1]
                    });
                    watch = closeId + 1;
                    continue;
                } else {
                    watch = openId;
                    continue;
                }
            } else {
                const openBraked = closeName + 1;
                if (text.indexOf(')', openBraked + 1) === -1) {
                    watch = openBraked + 1;
                    continue;
                }
                let urlStart = -1;
                for (let i = openBraked + 1; i < text.length; i++)
                    if (text[i] !== ' ') {
                        urlStart = i;
                        break;
                    }
                if (urlStart === -1)
                    break;

                const urlSpace = text.indexOf(' ', urlStart + 1);
                const urlBraked = text.indexOf(')', urlStart + 1);
                if (urlSpace === -1 && urlBraked === -1) {
                    watch = urlStart + 1;
                    continue;
                }

                let title;
                let endLink;

                let urlEnd = -1;
                let end = false;
                if (urlSpace === -1 || urlBraked < urlSpace) {
                    urlEnd = urlBraked;
                    end = true;
                    endLink = urlBraked;
                } else
                    urlEnd = urlSpace;

                const url = text.substring(urlStart, urlEnd);

                if (!end) {
                    let nextChar = -1;
                    for (let i = urlEnd + 1; i < text.length; i++)
                        if (text[i] !== ' ') {
                            nextChar = i;
                            break;
                        }
                    if (text[nextChar] !== '"' && text[nextChar] !== '\'' && text[nextChar] !== ')') {
                        watch = nextChar;
                        continue;
                    }
                    if (text[nextChar] === '"' || text[nextChar] === '\'') {
                        const quote = text[nextChar];
                        const closeQuote = text.indexOf(quote, nextChar + 1);
                        if (closeQuote === -1) {
                            watch = nextChar + 1;
                            continue;
                        }
                        title = text.substring(nextChar + 1, closeQuote);

                        const closeBraked = text.indexOf(')', closeQuote + 1);
                        if (closeBraked === -1) {
                            watch = closeQuote + 1;
                            continue;
                        }
                        if (!text.substring(closeQuote + 1, closeBraked).consistsOf(' ')) {
                            watch = closeQuote + 1;
                            continue;
                        }
                        endLink = closeBraked;
                    } else {
                        endLink = nextChar;
                    }
                }

                links.push({
                    startIndex: (isImg ? openName - 1 : openName),
                    endIndex: endLink,
                    isImg: isImg,
                    name: linkName,
                    url: url,
                    title: title
                });
                watch = endLink + 1;
            }
        }

        for (const link of links.reverse()) {
            let tag;
            if (link.isImg) {
                let attributes = {
                    src: link.url
                };
                if (link.name !== '')
                    attributes['alt'] = link.name;
                if (link.title)
                    attributes['title'] = link.title;
                tag = this.builder.BuildTag('img', attributes);
            } else {
                let attributes = {
                    href: link.url
                };
                if (link.title)
                    attributes['title'] = link.title;
                tag = this.builder.BuildTag('a', attributes, link.name);
            }

            const part1 = text.substring(0, link.startIndex);
            const part2 = text.substring(link.endIndex + 1);

            text = part1 + tag + part2;
        }

        return text;
    }

    CheckCode(text) {
        let blocks = [];

        let openIndex = -1;
        let double = false;
        for (let i = 0; i < text.length; i++) {
            let char = text[i];
            let next = text[i + 1] || '';
            if (openIndex === -1) {
                if (i + 2 === text.length)
                    break;
                if (char === '`') {
                    openIndex = i;
                    double = (next === '`');
                    if (double)
                        i++;
                }
            } else {
                if (char === '`') {
                    if (double && next === '`') {
                        blocks.push({
                            double: true,
                            start: openIndex,
                            end: i
                        });
                        openIndex = -1;
                        i++;
                    } else if (!double && next !== '`') {
                        blocks.push({
                            double: false,
                            start: openIndex,
                            end: i
                        });
                        openIndex = -1;
                    }
                }
            }
        }

        for (const block of blocks.reverse()) {
            const part1 = text.substring(0, block.start);
            const part2 = text.substring(block.start + (block.double ? 2 : 1), block.end);
            const part3 = text.substring(block.end + (block.double ? 2 : 1));

            const html = this.builder.BuildTag('code', null, part2);
            text = `${part1}${html}${part3}`;
        }

        return text;
    }

    Check(text, chars, tag) {
        const length = chars.length;
        if (text.length <= length * 2)
            return text;

        let watch = 0;
        while (true) {
            if (watch + length >= text.length)
                return text;

            let startIndex = text.indexOf(chars, watch);
            if (startIndex === -1)
                return text;
            if (startIndex + length === text.length || text.substr(startIndex + length, length) === chars || text[startIndex + length] === ' ' || text[startIndex + length] === '\t')
                return text;

            let endIndex;
            let startSearch = startIndex + length + 1;
            while (true) {
                endIndex = text.indexOf(chars, startSearch);
                if (endIndex === -1)
                    return text;
                if (text[endIndex - 1] === ' ' || text[endIndex - 1] === '\t') {
                    if (endIndex + length === text.length)
                        return text;
                    else if (text.substr(endIndex + length, length) === chars || text[endIndex + length] === ' ' || text[endIndex + length] === '\t') {
                        startSearch = endIndex + length + 1;
                    } else {
                        startIndex = endIndex;
                        startSearch = startIndex + length + 1;
                    }
                } else {
                    break;
                }
            }

            let part1 = text.substring(0, startIndex);
            let tags1 = part1.lonelyTags();

            let part2 = text.substring(startIndex + length, endIndex);
            let tags2 = part2.lonelyTags();

            let part3 = text.substring(endIndex + length);

            let cont = false;
            if (tags1.opening.includes('code'))
                cont = true;
            else for (const tag1 of tags1.opening)
                if (tags2.closing.includes(tag1)) {
                    cont = true;
                    break;
                }
            if (tags2.opening.length > 0) {
                cont = true;
            }
            if (cont) {
                watch = endIndex + length + 1;
                continue;
            }

            const html = this.builder.BuildTag(tag, null, part2);
            text = `${part1}${html}${part3}`;

            watch = `${part1}${html}`.length + 1;
        }
    }
}

class Block {
    Type = '';
    Content = [];
    Option;
    #builder;
    #parserOptions;
    #references;
    #extensions = {
        blockquote: {
            parseInner: true,
            ignoreSubPIfSingle: () => true,
        },
        li: {
            parseInner: true,
            ignoreSubPIfSingle: () => !(this.Option && this.Option['forceParse']),
            trimBlankLines: true,
        },
        code: {separator: '\r\n'},
        p: {parseSpan: true},
        h1: {parseSpan: true},
        h2: {parseSpan: true},
        h3: {parseSpan: true},
        h4: {parseSpan: true},
        h5: {parseSpan: true},
        h6: {parseSpan: true},
    };

    constructor(builder, options, references) {
        this.#builder = builder;
        this.#parserOptions = options;
        this.#references = references;
    }

    get Options() {
        const defaultExtension = {
            parseSpan: false,
            parseInner: false,
            ignoreSubPIfSingle: () => false,
            trimBlankLines: false,
            separator: ' '
        };
        if (Object.keys(this.#extensions).includes(this.Type)) {
            let extension = this.#extensions[this.Type];
            if (extension.parseSpan === undefined)
                extension.parseSpan = defaultExtension.parseSpan;
            if (extension.parseInner === undefined)
                extension.parseInner = defaultExtension.parseInner;
            if (extension.ignoreSubPIfSingle === undefined)
                extension.ignoreSubPIfSingle = defaultExtension.ignoreSubPIfSingle;
            if (extension.trimBlankLines === undefined)
                extension.trimBlankLines = defaultExtension.trimBlankLines;
            if (extension.separator === undefined)
                extension.separator = defaultExtension.separator;
            return extension;
        } else
            return defaultExtension;
    }

    toString() {
        let content = this.Content;
        if (this.Options.trimBlankLines)
            content = content.trim();

        if (this.Type === 'html') {
            return this.Content.join('\r\n');
        } else if (this.Options.parseInner) {
            const htmlParser = new HtmlParserInternal(content.join('\n'), this.#builder);
            htmlParser.Options = this.#parserOptions;
            htmlParser.IgnoreTagIfSingleP = this.Options.ignoreSubPIfSingle();
            const html = htmlParser.Parse();
            const attributes = (this.Option && this.Option['attributes']) ? this.Option['attributes'] : null;
            return this.#builder.BuildTag(this.Type, attributes, html.trim());
        } else if (this.Option && this.Option['ignoreSurroundingTag']) {
            let inline = this.Content.join(this.Options.separator).trim();
            if (this.Options.parseSpan) {
                let parser = new InlineParser(inline, this.#builder, this.#references);
                inline = parser.Parse();
            }
            return inline;
        } else {
            let inline = this.Content.join(this.Options.separator).trim();
            if (this.Options.parseSpan) {
                let parser = new InlineParser(inline, this.#builder, this.#references);
                inline = parser.Parse();
            }
            const attributes = (this.Option && this.Option['attributes']) ? this.Option['attributes'] : null;
            return this.#builder.BuildTag(this.Type, attributes, inline);
        }
    }
}
