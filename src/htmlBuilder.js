﻿class HtmlBuilder {
  #builder;
  #numbering = {
    enabled: false,
    h1: 0,
    h2: 0,
    h3: 0,
    h4: 0,
    h5: 0,
    h6: 0,
  };

  constructor(templates) {
    this.#builder = new HtmlBuilderInternal(templates, this.#numbering);
  }

  BuildTag(tag, attributes, content) {
    return this.#builder.BuildTag(tag, attributes, content);
  }

  BuildPage(content) {
    return this.#builder.BuildPage(content);
  }

  ResetHNumbers() {
    this.#numbering.enabled = false;
    this.#numbering.h1 = 0;
    this.#numbering.h2 = 0;
    this.#numbering.h3 = 0;
    this.#numbering.h4 = 0;
    this.#numbering.h5 = 0;
    this.#numbering.h6 = 0;
  }

  EnableHNumbering(enabled) {
    this.#numbering.enabled = enabled;
  }
}

module.exports = HtmlBuilder;

class HtmlBuilderInternal {
  doNotClose = ['area', 'base', 'br', 'col', 'command', 'embed', 'hr', 'img', 'input', 'keygen', 'link', 'meta', 'param', 'source', 'track', 'wbr'];
  templates;

  numbering;

  constructor(templates, numbering) {
    this.templates = templates;
    this.numbering = numbering;
  }

  BuildTag(tag, attributes, content) {
    if (!tag || tag.trim() === '')
      throw 'Tag cannot be null or empty';

    let template = this.getTagTemplate(tag);
    if (content) {
      if (this.numbering.enabled && (tag === 'h1' || tag === 'h2' || tag === 'h3' || tag === 'h4' || tag === 'h5' || tag === 'h6')) {
        if (tag === 'h1') {
          this.numbering.h1++;
          this.numbering.h2 = 0;
          this.numbering.h3 = 0;
          this.numbering.h4 = 0;
          this.numbering.h5 = 0;
          this.numbering.h6 = 0;
        }
        else if (tag === 'h2') {
          this.numbering.h2++;
          this.numbering.h3 = 0;
          this.numbering.h4 = 0;
          this.numbering.h5 = 0;
          this.numbering.h6 = 0;
        }
        else if (tag === 'h3') {
          this.numbering.h3++;
          this.numbering.h4 = 0;
          this.numbering.h5 = 0;
          this.numbering.h6 = 0;
        }
        else if (tag === 'h4') {
          this.numbering.h4++;
          this.numbering.h5 = 0;
          this.numbering.h6 = 0;
        }
        else if (tag === 'h5') {
          this.numbering.h5++;
          this.numbering.h6 = 0;
        }
        else if (tag === 'h6') {
          this.numbering.h6++;
        }

        const number = this.getHeadingNumberingTemplate(tag, this.numbering.h1, this.numbering.h2, this.numbering.h3, this.numbering.h4, this.numbering.h5, this.numbering.h6);

        content = number + content;
      }

      template = template.replace('${CONTENT}', content);
    } else {
      template = template.replace('${CONTENT}', '');
    }
    let attributeString = '';
    if (attributes && typeof attributes === 'object') {
      for (const entry of Object.entries(attributes)){
        attributeString += ' ';
        attributeString += entry[0];
        if (entry[1]) {
          let value = entry[1];
          value = value.replace(/([^\\]|^)"/gm, '$1\\"');
          attributeString += `="${value}"`;
        }
      }
    }
    template = template.replace('${ATTRIBUTES}', attributeString);
    return template;
  }

  BuildPage(content) {
    let template = this.getHtmlTemplate();
    template = template.replace('${CONTENT}', content);
    return template;
  }

  getTagTemplate(tag) {
    if (this.templates && Object.keys(this.templates).includes(tag))
      return this.templates[tag];
    else if (this.doNotClose.includes(tag))
      return '<' + tag + '${ATTRIBUTES} />';
    else
      return '<' + tag + '${ATTRIBUTES}>${CONTENT}</' + tag + '>';
  }

  getHtmlTemplate() {
    if (this.templates && this.templates['html']) {
      return this.templates['html'];
    } else {
      return '<!DOCTYPE html><html><body>\r\n${CONTENT}\r\n</body></html>\r\n';
    }
  }

  getHeadingNumberingTemplate(type, l1, l2, l3, l4, l5, l6) {
    let templates = {
      h1: '',
      h2: '',
      h3: '',
      h4: '',
      h5: '',
      h6: ''
    };

    if (this.templates && this.templates['hNumbering']) {
      if (this.templates['hNumbering']['level1'])
        templates.h1 = this.templates['hNumbering']['level1'];
      if (this.templates['hNumbering']['level2'])
        templates.h2 = this.templates['hNumbering']['level2'];
      if (this.templates['hNumbering']['level3'])
        templates.h3 = this.templates['hNumbering']['level3'];
      if (this.templates['hNumbering']['level4'])
        templates.h4 = this.templates['hNumbering']['level4'];
      if (this.templates['hNumbering']['level5'])
        templates.h5 = this.templates['hNumbering']['level5'];
      if (this.templates['hNumbering']['level6'])
        templates.h6 = this.templates['hNumbering']['level6'];
    } else {
      templates = {
        h1: '${l1}. ',
        h2: '${l1}.${l2}. ',
        h3: '${l1}.${l2}.${l3}. ',
        h4: '${l1}.${l2}.${l3}.${l4}. ',
        h5: '${l1}.${l2}.${l3}.${l4}.${l5}. ',
        h6: '${l1}.${l2}.${l3}.${l4}.${l5}.${l6}. '
      };
    }

    let template = templates[type];
    if (!template)
      return '';

    template = template.replace('${l1}', l1);
    template = template.replace('${l2}', l2);
    template = template.replace('${l3}', l3);
    template = template.replace('${l4}', l4);
    template = template.replace('${l5}', l5);
    template = template.replace('${l6}', l6);

    return template;
  }
}
